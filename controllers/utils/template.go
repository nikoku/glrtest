package utils

import (
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Component represents an application / micro-service
// that makes part of a larger application
type Component struct {
	// Namespace of the component
	Namespace string
	// Defines the number of pods for the
	// component to be created
	Replicas int32
	// Labels for the component
	Labels map[string]string
	// InitContainers contains a list of containers	that may
	// need to run before the main application container starts up
	InitContainers []corev1.Container
	// Containers containers a list of containers that make up a pod
	Containers []corev1.Container
	// Contains a list of volumes used by the containers
	Volumes []corev1.Volume
	// Defines volume claims to be used by a statefulset
	VolumeClaimTemplates []corev1.PersistentVolumeClaim
}

// GenericDeployment returns a generic deployment
func GenericDeployment(component Component) *appsv1.Deployment {
	var replicas int32
	labels := component.Labels

	if component.Replicas != 0 {
		replicas = component.Replicas
	} else {
		replicas = 1
	}

	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      labels["app.kubernetes.io/instance"],
			Namespace: component.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					InitContainers: component.InitContainers,
					Containers:     component.Containers,
					Volumes:        component.Volumes,
				},
			},
		},
	}
}

// ServiceAccount returns service account to be used by pods
func ServiceAccount(name, namespace string) *corev1.ServiceAccount {
	return &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels: map[string]string{
				"app.kubernetes.io/name":       name,
				"app.kubernetes.io/created-by": "gitlab-runner-operator",
			},
		},
	}
}

// GenericSecret returns empty secret
func GenericSecret(name, namespace string, labels map[string]string) *corev1.Secret {
	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    labels,
		},
		StringData: map[string]string{},
	}
}

// GenericConfigMap returns empty configmap
func GenericConfigMap(name, namespace string, labels map[string]string) *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    labels,
		},
		Data: map[string]string{},
	}
}
