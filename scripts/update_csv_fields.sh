#!/bin/bash

set -eo pipefail

# TODO: When this script is rewritten in python accessing tools will be more automatic
# For now, let's allow using yq as usual from the .tmp directory
PATH=$PATH:.tmp

CSV_PATH="bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml"

# Set current and previous version

DEFAULT_CURRENT_RELEASE=$(./scripts/ci/versions.py current)
CURRENT_RELEASE="${1:-$DEFAULT_CURRENT_RELEASE}"
PREVIOUS_RELEASE=$(./scripts/ci/versions.py previous)

if ! command -v yq &> /dev/null
then
    echo "yq is not installed. Download v4 from https://github.com/mikefarah/yq"
    exit 1
fi

IMAGE=$(yq e .metadata.name $CSV_PATH | cut -f1 -d".")
yq e ".metadata.name = \"$IMAGE.$CURRENT_RELEASE\"" -i $CSV_PATH
if [[ -z "$PREVIOUS_RELEASE" ]]; then
  yq e "del(.spec.replaces)" -i $CSV_PATH
else
  yq e ".spec.replaces = \"$IMAGE.$PREVIOUS_RELEASE\"" -i $CSV_PATH
fi

echo "CSV .metadata.name set to $(yq e .metadata.name $CSV_PATH)"
echo "CSV .spec.replaces set to $(yq e .spec.replaces $CSV_PATH)"

# Set createdAt date

DATE_RFC3339=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
yq e ".metadata.annotations.createdAt = \"$DATE_RFC3339\"" -i $CSV_PATH

echo "CSV .metadata.annotations.createdAt set to $(yq e .metadata.annotations.createdAt $CSV_PATH)"